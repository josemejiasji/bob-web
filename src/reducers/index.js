import {combineReducers} from 'redux';
import orders from './orderReducer';

const rootReducer = combineReducers({
  // short hand property names
  orders
});

export default rootReducer;