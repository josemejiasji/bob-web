import {REQUEST_ORDERS, RECEIVE_ORDERS} from '../actions/orderActions';

const initialState = {
  isFetching: false,
  items: []
};

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_ORDERS:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_ORDERS:
      return Object.assign({}, state, {
        isFetching: false,
        items: action.payload.orders,
      });
    default:
      return state
  }
};

export default orderReducer;
