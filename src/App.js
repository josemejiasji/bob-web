import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchOrders} from './actions/orderActions';
import Card from './components/Card';
import './App.scss';

class App extends Component {
  componentDidMount() {
    this.props.dispatch(fetchOrders());
  }

  render() {
    const {isFetching, orders} = this.props;

    if (isFetching) {
      return <div>Loading orders...</div>;
    }

    return (
      <div className="cards">
        {orders.map(order => <Card key={order.id} name={order.name} bags={order.bags}/>)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isFetching: state.orders.isFetching,
    orders: state.orders.items
  };
};

export default connect(mapStateToProps)(App);
