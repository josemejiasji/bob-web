import React, {Component} from 'react';

export default class Card extends Component {
  render() {
    const {name, bags} = this.props;

    return (
      <div className="cards__item">
        <div className="card">
          <h4>{name}</h4>
          <p>{bags}</p>
        </div>
      </div>
    )
  }
}
