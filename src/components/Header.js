import React, {Component} from 'react';

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <div className="header__logo">
          Bob.io
        </div>
      </div>
    )
  }
}
