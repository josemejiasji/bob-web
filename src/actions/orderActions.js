import fetch from 'isomorphic-fetch';

export const REQUEST_ORDERS = 'REQUEST_ORDERS';
export const RECEIVE_ORDERS = 'RECEIVE_ORDERS';

function requestOrders() {
  return {
    type: REQUEST_ORDERS
  }
}

function receiveOrders(orders) {
  return {
    type: RECEIVE_ORDERS,
    payload: {orders}
  }
}

export function fetchOrders() {
  return dispatch => {
    dispatch(requestOrders());
    return fetch(`http://localhost:3001/api/orders`)
      .then(req => req.json())
      .then(json => dispatch(receiveOrders(json)));
  }
}